import { Injectable, Inject } from '@angular/core';
import { Phrase } from '../models/phrase';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase/app';

@Injectable()
export class PhraseService {

  constructor(private db: AngularFirestore) {
  }

  readAll(): Observable<Phrase[]>{
    return this.db.collection("phrases", ref => ref.orderBy("timestamp", "desc")).snapshotChanges().map(actions => {
      return actions.map(a => {
          return a.payload.doc.data() as Phrase;
        }
      );
    });
  }

  create(phrase): Promise<void>{
    const timestamp = firebase.firestore.FieldValue.serverTimestamp();
    console.log(timestamp);
    phrase.createdAt = timestamp;
    phrase.modifiedAt = timestamp;    
    phrase.timestamp = (new Date()).getTime()
    let id = this.db.createId();
    phrase.id = id;
    return this.db.collection('phrases').doc(id).set(phrase);
  }

  update(phrase): Promise<void>{
    phrase.modifiedAt = firebase.firestore.FieldValue.serverTimestamp();
    if(!phrase.timestamp){
      phrase.timestamp = (new Date()).getTime();
    }
    return this.db.collection('phrases').doc(phrase.id).update(phrase);
  }

  updateAll(phrases): Promise<void>{
    const batch = this.db.firestore.batch();
    for(var i=0; i<phrases.length; i++){
      phrases[i].modifiedAt = firebase.firestore.FieldValue.serverTimestamp();
      if(!phrases[i].timestamp){
        phrases[i].timestamp = (new Date()).getTime();
      }
      batch.update(firebase.firestore().doc("phrases/" + phrases[i].id), phrases[i]);
    }
    return batch.commit();
  }

  updateMix(list): Promise<void>{
    const batch = this.db.firestore.batch();
    for(let item of list){
      batch.update(firebase.firestore().doc("phrases/" + item.id), {timestamp: item.timestamp});            
    }
    return batch.commit();
  }

  delete(id): Promise<void>{
    return this.db.collection('phrases').doc(id).delete();
  }

  read(id): Observable<Phrase>{
    return this.db.collection('phrases').doc(id).snapshotChanges().map(actions => {
      return actions.payload.data() as Phrase;
    });
  }

}
