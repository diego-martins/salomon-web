import { Injectable } from '@angular/core';
import { Author } from '../models/author';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase/app';

@Injectable()
export class AuthorService {

  constructor(private db: AngularFirestore) { }

  readAll(): Observable<Author[]>{
    return this.db.collection("authors", ref => ref.orderBy("modifiedAt", "desc")).snapshotChanges().map(actions => {
      return actions.map(a => {
          return a.payload.doc.data() as Author;
        }
      );
    });
  }

  create(author){
    const timestamp = firebase.firestore.FieldValue.serverTimestamp();
    author.createdAt = timestamp;
    author.modifiedAt = timestamp;
    author.timestamp = (new Date()).getTime();
    let id = this.db.createId();
    author.id = id;
    this.db.collection('authors').doc(id).set(author);
  }

  update(author){
    author.modifiedAt = firebase.firestore.FieldValue.serverTimestamp();
    if(!author.timestamp){
      author.timestamp = (new Date()).getTime();
    }
    this.db.collection('authors').doc(author.id).update(author);
  }

  delete(id){
    this.db.collection('authors').doc(id).delete();
  }

  read(id): Observable<Author>{
    return this.db.collection('authors').doc(id).snapshotChanges().map(actions => {
      return actions.payload.data() as Author;
    });
  }
}
