export class Lang {
    constructor(
        public id: string,
        public name: string
    ){}
}
