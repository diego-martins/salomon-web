export class Phrase {
    constructor(
        public id: string,
        public text: string,
        public authorId: string,
        public authorName: string,
        public timestamp: number,
        public createdAt: string,
        public modifiedAt: string,
        public lang: string,
        public imageUrl: string,
        public enabled: boolean
    ){}
}
