export class Author {
    constructor(
        public id: string,
        public nameEn: string,
        public namePt: string,
        public nameEs: string,
        public aboutEn: string,
        public aboutPt: string,
        public aboutEs: string,
        public timestamp: number,
        public imageUrl: string,
        public createdAt: string,
        public modifiedAt: string,
        public enabled: boolean
        ){}
}