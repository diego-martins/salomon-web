import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ReadPhrasesComponent } from './admin/phrases/read-phrases/read-phrases.component';
import { CreatePhraseComponent } from './admin/phrases/create-phrase/create-phrase.component';
import { ReadPhraseComponent } from './admin/phrases/read-phrase/read-phrase.component';
import { DeletePhraseComponent } from './admin/phrases/delete-phrase/delete-phrase.component';
import { UpdatePhraseComponent } from './admin/phrases/update-phrase/update-phrase.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PhrasesComponent } from './admin/phrases/phrases.component';
import { AuthorsComponent } from './admin/authors/authors.component';
import { CreateAuthorComponent } from './admin/authors/create-author/create-author.component';
import { DeleteAuthorComponent } from './admin/authors/delete-author/delete-author.component';
import { ReadAuthorComponent } from './admin/authors/read-author/read-author.component';
import { ReadAuthorsComponent } from './admin/authors/read-authors/read-authors.component';
import { UpdateAuthorComponent } from './admin/authors/update-author/update-author.component';
import { LoginEmailComponent } from './user/login-email/login-email.component'

import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    ReadPhrasesComponent,
    CreatePhraseComponent,
    ReadPhraseComponent,
    DeletePhraseComponent,
    UpdatePhraseComponent,
    PhrasesComponent,
    AuthorsComponent,
    CreateAuthorComponent,
    DeleteAuthorComponent,
    ReadAuthorComponent,
    ReadAuthorsComponent,
    UpdateAuthorComponent,
    LoginEmailComponent
    ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    RouterModule.forRoot([
      {
        path: "admin/phrases",
        component: PhrasesComponent,
        canActivate: [AuthGuardService]
      },
      {
        path: "admin/authors",
        component: AuthorsComponent,
        canActivate: [AuthGuardService]
      },
      {
        path: "user/login-email",
        component: LoginEmailComponent
      }
    ])    
  ],
  providers: [AuthService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
