import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login-email',
  templateUrl: './login-email.component.html',
  styleUrls: ['./login-email.component.css']
})
export class LoginEmailComponent implements OnInit {

  email: string;
  password: string;

  constructor(public authService: AuthService) {}

  login() {
    this.authService.login(this.email, this.password, "/admin/phrases");
    this.password = '';    
  }

  logout() {
    this.authService.logout();
  }

  ngOnInit() {
  }

}
