import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletePhraseComponent } from './delete-phrase.component';

describe('DeletePhraseComponent', () => {
  let component: DeletePhraseComponent;
  let fixture: ComponentFixture<DeletePhraseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeletePhraseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletePhraseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
