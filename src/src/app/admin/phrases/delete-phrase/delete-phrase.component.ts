import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PhraseService } from '../../../services/phrase.service';
import { Observable } from 'rxjs/Observable';
import { Phrase } from '../../../models/phrase';

@Component({
  selector: 'app-delete-phrase',
  templateUrl: './delete-phrase.component.html',
  styleUrls: ['./delete-phrase.component.css'],
  providers: [PhraseService]
})
export class DeletePhraseComponent implements OnInit {

  @Output() show_read_phrases_event = new EventEmitter();
  @Input() id;

  constructor(private phraseService: PhraseService) { }

  delete(){
    this.phraseService.delete(this.id);
    this.readAll();
  }

  readAll(){
    this.show_read_phrases_event.emit({title: "Read Phrases"});
  }

  ngOnInit() {
  }

}
