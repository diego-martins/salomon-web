import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-phrases',
  templateUrl: './phrases.component.html',
  styleUrls: ['./phrases.component.css']
})
export class PhrasesComponent implements OnInit {

  title = 'Read Phrases';
  id;
  show_create_phrase_html=false;
  show_read_phrases_html=true;
  show_read_phrase_html=false;
  show_update_phrase_html=false;
  show_delete_phrase_html=false;
  
  constructor(public authService: AuthService, private router: Router) {
   }

  logout() {
    this.authService.logout();
    this.router.navigate(["/user/login-email"]);
  }

  ngOnInit() {
  }

  showCreatePhrase($event){
      this.title=$event.title;
  
      this.hideAllHtml();
      this.show_create_phrase_html=true;
  }
  
  showReadPhrases($event){
      this.title=$event.title;
  
      this.hideAllHtml();
      this.show_read_phrases_html=true;
  }
  
  showReadPhrase($event){
      this.title=$event.title;
      this.id=$event.id;
      
      this.hideAllHtml();
      this.show_read_phrase_html=true;
  }

  showUpdatePhrase($event){    
      this.title=$event.title;
      this.id=$event.id;
      
      this.hideAllHtml();
      this.show_update_phrase_html=true;
  }

  showDeletePhrase($event){
      this.title=$event.title;
      this.id=$event.id;
  
      this.hideAllHtml();
      this.show_delete_phrase_html=true;
  }
  
  hideAllHtml(){
      this.show_create_phrase_html=false;
      this.show_read_phrases_html=false;
      this.show_read_phrase_html=false;
      this.show_update_phrase_html=false;
      this.show_delete_phrase_html=false;
  }

}
