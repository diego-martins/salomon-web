import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadPhraseComponent } from './read-phrase.component';

describe('ReadPhraseComponent', () => {
  let component: ReadPhraseComponent;
  let fixture: ComponentFixture<ReadPhraseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadPhraseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadPhraseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
