import { Component, Input, Output, OnChanges, EventEmitter } from '@angular/core';
import { PhraseService } from '../../../services/phrase.service';
import { Observable } from 'rxjs/Observable';
import { Phrase } from '../../../models/phrase';

@Component({
  selector: 'app-read-phrase',
  templateUrl: './read-phrase.component.html',
  styleUrls: ['./read-phrase.component.css'],
  providers: [PhraseService]
})
export class ReadPhraseComponent implements OnChanges {

  @Output() show_read_phrases_event = new EventEmitter();
  
  @Input() id;
  model: Phrase;

  constructor(private phraseService: PhraseService) { }

  readAll(){
    this.show_read_phrases_event.emit({title: "Read Phrases"});
  }

  ngOnChanges() {
    this.phraseService.read(this.id).subscribe(model => this.model = model);
  }

  getLangName(lang: string){
    if(lang == "en"){
      return "English";
    }
    else if(lang == "pt"){
      return "Portuguese";
    }
    else if(lang == "es"){
      return "Spanish";
    }
    else {
      return "";
    }
  }
  
}
