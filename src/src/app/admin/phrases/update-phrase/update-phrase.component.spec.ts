import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePhraseComponent } from './update-phrase.component';

describe('UpdatePhraseComponent', () => {
  let component: UpdatePhraseComponent;
  let fixture: ComponentFixture<UpdatePhraseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePhraseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePhraseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
