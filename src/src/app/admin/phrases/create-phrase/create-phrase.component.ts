import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PhraseService } from '../../../services/phrase.service';
import { AuthorService } from '../../../services/author.service';
import { Observable } from 'rxjs/Observable';
import { Phrase } from '../../../models/phrase';
import { Author } from '../../../models/author';
import { Lang } from '../../../models/lang';

@Component({
  selector: 'app-create-phrase',
  templateUrl: './create-phrase.component.html',
  styleUrls: ['./create-phrase.component.css'],
  providers: [PhraseService, AuthorService]
})

export class CreatePhraseComponent implements OnInit {

  form: FormGroup;
  authors: Author[];
  langs: Lang[];

  @Output() show_read_phrases_event = new EventEmitter();

  constructor(
    private phraseService: PhraseService, 
    private authorService: AuthorService,
    formBuilder: FormBuilder) { 
    this.form = formBuilder.group({
      text: ["", Validators.required],
      authorId: ["", Validators.required],
      lang: ["", Validators.required],
      imageUrl: ["", Validators.nullValidator],
      enabled: ["", Validators.nullValidator]
    });
  }

  create(){

    for(let author of this.authors){
      if(author.id == this.form.value.authorId){
        if(this.form.value.lang == "pt"){
          this.form.value.authorName = author.namePt || author.nameEn;
        }
        else if(this.form.value.lang == "es"){
          this.form.value.authorName = author.nameEs || author.nameEn;
        }
        else {
          this.form.value.authorName = author.nameEn;
        }        
        break;
      }
    }

    this.phraseService.create(this.form.value);
    /*
      .subscribe(
        phrase => {
          console.log(phrase);
          this.readPhrases();
        },
        error => console.log(error)
      )
      */
      this.readAll();
  }

  readAll(){
    this.show_read_phrases_event.emit({title: "Read Phrases"});
  }

  ngOnInit() {
    
    this.langs = [new Lang("en", "English"), new Lang("pt", "Portuguese"), new Lang("es", "Spanish")];
    this.authorService.readAll()
    .subscribe(
      models => this.authors = models
    ); 

  }

  getLangName(lang: string){
    if(lang == "en"){
      return "English";
    }
    else if(lang == "pt"){
      return "Portuguese";
    }
    else if(lang == "es"){
      return "Spanish";
    }
    else {
      return "";
    }
  }
}
