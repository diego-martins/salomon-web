import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PhraseService } from '../../../services/phrase.service';
import { Observable } from 'rxjs/Observable';
import { Phrase } from '../../../models/phrase';
import { Router } from '@angular/router';

@Component({
  selector: 'app-read-phrases',
  templateUrl: './read-phrases.component.html',
  styleUrls: ['./read-phrases.component.css'],
  providers: [PhraseService]
})
export class ReadPhrasesComponent implements OnInit {
  
  items: Phrase[];

  @Output() show_create_phrase_event=new EventEmitter();
  @Output() show_read_phrase_event=new EventEmitter();
  @Output() show_update_phrase_event=new EventEmitter();
  @Output() show_delete_phrase_event=new EventEmitter();

  constructor(private phraseService: PhraseService, private router: Router) {     
  }

  authors(){
    this.router.navigate(["/admin/authors"]);
  }

  create(){
    this.show_create_phrase_event.emit({
      title: "Create Phrase"
    });
  }

  read(id){
    this.show_read_phrase_event.emit({
      id: id,
      title: "Read Phrase"
    });
  }

  update(id){
    this.show_update_phrase_event.emit({
      id: id,
      title: "Update Phrase"
    });
  }

  delete(id){
    this.show_delete_phrase_event.emit({
      id: id,
      title: "Delete Phrase"
    });
  }

  ngOnInit() {
    this.phraseService.readAll().subscribe(items => this.items = items);
  }

  getLangName(lang: string){
    if(lang == "en"){
      return "English";
    }
    else if(lang == "pt"){
      return "Portuguese";
    }
    else if(lang == "es"){
      return "Spanish";
    }
    else {
      return "";
    }
  }
  
  swapItems = Array<Phrase>();

  isChecked(item: Phrase){
    return this.swapItems.indexOf(item) != -1;
  }

  isDisabled(item: Phrase){
    return this.swapEnabled() && this.swapItems.indexOf(item) == -1;
  }

  swapEnabled(){
    return this.swapItems.length == 2;
  }

  mix(){
    var length = this.items.length;
    for(let fromIndex=0; fromIndex<length; fromIndex++){
      var toIndex = Math.floor(Math.random() * length);
      var toTimestamp = this.items[toIndex].timestamp;
      this.items[toIndex].timestamp = this.items[fromIndex].timestamp;
      this.items[fromIndex].timestamp = toTimestamp;
    }
    this.saveMix();
  }

  saveMix(){
    var length = this.items.length;
    var list = [];
    for(let i=0; i<length; i++){
      list.push({
        id: this.items[i].id,
        timestamp: this.items[i].timestamp
      });
    }
    this.phraseService.updateMix(list);
  }

  markToSwap(item: Phrase){
    var index = this.swapItems.indexOf(item);
    if(index == -1){
      this.swapItems.push(item);
    }else{
      this.swapItems.splice(index, 1);
    }

    if(this.swapEnabled()){
      var timestamp = this.swapItems[0].timestamp;
      this.swapItems[0].timestamp = this.swapItems[1].timestamp;
      this.swapItems[1].timestamp = timestamp;
      this.phraseService.updateAll(this.swapItems);
      this.phraseService.readAll().subscribe(items => this.items = items);
      this.swapItems = [];
    }

  }

}
