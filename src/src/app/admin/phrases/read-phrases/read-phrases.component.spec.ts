import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadPhrasesComponent } from './read-phrases.component';

describe('ReadPhrasesComponent', () => {
  let component: ReadPhrasesComponent;
  let fixture: ComponentFixture<ReadPhrasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadPhrasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadPhrasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
