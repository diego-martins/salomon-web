import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadAuthorsComponent } from './read-authors.component';

describe('ReadAuthorsComponent', () => {
  let component: ReadAuthorsComponent;
  let fixture: ComponentFixture<ReadAuthorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadAuthorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadAuthorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
