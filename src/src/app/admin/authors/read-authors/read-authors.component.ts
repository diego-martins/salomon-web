import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthorService } from '../../../services/author.service';
import { Observable } from 'rxjs/Observable';
import { Author } from '../../../models/author';
import { Router } from '@angular/router';

@Component({
  selector: 'app-read-authors',
  templateUrl: './read-authors.component.html',
  styleUrls: ['./read-authors.component.css'],
  providers: [AuthorService]
})
export class ReadAuthorsComponent implements OnInit {

  @Output() show_create_author_event=new EventEmitter();
  @Output() show_read_author_event=new EventEmitter();
  @Output() show_update_author_event=new EventEmitter();
  @Output() show_delete_author_event=new EventEmitter();

  items: Observable<Author[]>;

  constructor(private authorService: AuthorService, private router: Router) { }

  phrases(){
    this.router.navigate(["/admin/phrases"]);
  }

  create(){
    this.show_create_author_event.emit({
      title: "Create Author"
    });
  }

  read(id){
    this.show_read_author_event.emit({
      id: id,
      title: "Read Author"
    });
  }

  update(id){
    this.show_update_author_event.emit({
      id: id,
      title: "Update Author"
    });
  }

  delete(id){
    this.show_delete_author_event.emit({
      id: id,
      title: "Delete Author"
    });
  }

  ngOnInit() {
    this.items = this.authorService.readAll();
  }

}
