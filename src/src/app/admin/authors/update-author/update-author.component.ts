import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthorService } from '../../../services/author.service';
import { Author } from '../../../models/author';

@Component({
  selector: 'app-update-author',
  templateUrl: './update-author.component.html',
  styleUrls: ['./update-author.component.css'],
  providers: [AuthorService]
})

export class UpdateAuthorComponent implements OnChanges {

  form: FormGroup;
  @Output() show_read_authors_event = new EventEmitter();
  @Input() id;
  model: Author;

  constructor(
    private authorService: AuthorService,
    private formBuilder: FormBuilder) { 
    this.form = formBuilder.group({
      nameEn: ["", Validators.required],
      namePt: ["", Validators.nullValidator],
      nameEs: ["", Validators.nullValidator],
      aboutEn: ["", Validators.required],
      aboutPt: ["", Validators.nullValidator],
      aboutEs: ["", Validators.nullValidator],
      imageUrl: ["", Validators.nullValidator],
      enabled: ["", Validators.nullValidator]
    });
  }

  update(){
    this.form.value.id = this.id;
    this.authorService.update(this.form.value);
    this.readAll();
  }

  readAll(){
    this.show_read_authors_event.emit({title: "Read Authors"});
  }

  ngOnChanges() {
    this.authorService.read(this.id)
    .subscribe(model => {
        this.form.patchValue({
            id: model.id,
            nameEn: model.nameEn,
            namePt: model.namePt,
            nameEs: model.nameEs,
            aboutEn: model.aboutEn,
            aboutPt: model.aboutPt,
            aboutEs: model.aboutEs,
            imageUrl: model.imageUrl,
            enabled: model.enabled
        });

    });
  }

  ngOnInit(){   
  }

}
