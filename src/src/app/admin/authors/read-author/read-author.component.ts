import { Component, Input, Output, OnChanges, EventEmitter } from '@angular/core';
import { AuthorService } from '../../../services/author.service';
import { Observable } from 'rxjs/Observable';
import { Author } from '../../../models/author';

@Component({
  selector: 'app-read-author',
  templateUrl: './read-author.component.html',
  styleUrls: ['./read-author.component.css'],
  providers: [AuthorService]
})
export class ReadAuthorComponent implements OnChanges {

  @Output() show_read_authors_event = new EventEmitter();
  
  @Input() id;
  model: Author;

  constructor(private authorService: AuthorService) { }

  readAll(){
    this.show_read_authors_event.emit({title: "Read Authors"});
  }

  ngOnChanges() {
    this.authorService.read(this.id).subscribe(model => this.model = model);
  }
  
}
