import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthorService } from '../../../services/author.service';
import { Observable } from 'rxjs/Observable';
import { Author } from '../../../models/author';

@Component({
  selector: 'app-delete-author',
  templateUrl: './delete-author.component.html',
  styleUrls: ['./delete-author.component.css'],
  providers: [AuthorService]
})
export class DeleteAuthorComponent implements OnInit {

  @Output() show_read_authors_event = new EventEmitter();
  @Input() id;

  constructor(private authorService: AuthorService) { }

  delete(){
    this.authorService.delete(this.id);
    this.readAll();
  }

  readAll(){
    this.show_read_authors_event.emit({title: "Read Authors"});
  }

  ngOnInit() {
  }

}
