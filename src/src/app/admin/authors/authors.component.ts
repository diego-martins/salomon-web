import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  title = 'Read Authors';
  id;
  show_create_author_html=false;
  show_read_authors_html=true;
  show_read_author_html=false;
  show_update_author_html=false;
  show_delete_author_html=false;

  constructor(public authService: AuthService, private router: Router) { }

  logout() {
    this.authService.logout();
    this.router.navigate(["/user/login-email"]);
  }

  ngOnInit() {
  }

  showCreateAuthor($event){
    this.title=$event.title;

    this.hideAllHtml();
    this.show_create_author_html=true;
  }

  showReadAuthors($event){
      this.title=$event.title;

      this.hideAllHtml();
      this.show_read_authors_html=true;
  }

  showReadAuthor($event){
      this.title=$event.title;
      this.id=$event.id;
      
      this.hideAllHtml();
      this.show_read_author_html=true;
  }

  showUpdateAuthor($event){    
      this.title=$event.title;
      this.id=$event.id;
      
      this.hideAllHtml();
      this.show_update_author_html=true;
  }

  showDeleteAuthor($event){
      this.title=$event.title;
      this.id=$event.id;

      this.hideAllHtml();
      this.show_delete_author_html=true;
  }

  hideAllHtml(){
      this.show_create_author_html=false;
      this.show_read_authors_html=false;
      this.show_read_author_html=false;
      this.show_update_author_html=false;
      this.show_delete_author_html=false;
  }

}
