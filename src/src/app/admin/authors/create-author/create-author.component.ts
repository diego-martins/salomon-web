import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthorService } from '../../../services/author.service';
import { Observable } from 'rxjs/Observable';
import { Author } from '../../../models/author';

@Component({
  selector: 'app-create-author',
  templateUrl: './create-author.component.html',
  styleUrls: ['./create-author.component.css'],
  providers: [AuthorService]
})
export class CreateAuthorComponent implements OnInit {

  @Output() show_read_authors_event = new EventEmitter();

  form: FormGroup;

  constructor(private authorService: AuthorService, formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      nameEn: ["", Validators.required],
      namePt: ["", Validators.nullValidator],
      nameEs: ["", Validators.nullValidator],
      aboutEn: ["", Validators.required],
      aboutPt: ["", Validators.nullValidator],
      aboutEs: ["", Validators.nullValidator],
      imageUrl: ["", Validators.nullValidator],
      enabled: ["", Validators.nullValidator]
    });
   }

    create(){    
      this.authorService.create(this.form.value);
      this.readAll();
    }
  
    readAll(){
      this.show_read_authors_event.emit({title: "Read Authors"});
    }
  
    ngOnInit() {
    }
}
